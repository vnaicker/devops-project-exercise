FROM python as base
RUN apt-get update
RUN apt-get install -y curl
RUN curl -sSL https://install.python-poetry.org | python3 -
RUN apt-get update -qqy && apt-get install -qqy wget gnupg unzip
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
  && echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list \
  && apt-get update -qqy \
  && apt-get -qqy install google-chrome-stable \
  && rm /etc/apt/sources.list.d/google-chrome.list \
  && rm -rf /var/lib/apt/lists/* /var/cache/apt/*
ENV PATH="${PATH}:/root/.local/bin"
RUN mkdir /opt/todoapp
WORKDIR /opt/todoapp
COPY ./ ./
## Mounting a volume automatically creates the target folder, so you can delete this line once that is set up
RUN mkdir /var/log/todoapp
# Ensure poetry sets up all dependencies
RUN poetry config virtualenvs.create false --local && poetry install
ENV TODOAPP_PORT=5000
EXPOSE ${TODOAP_PORT}

FROM base as production
ENTRYPOINT poetry run gunicorn --bind 0.0.0.0 "todo_app.app:create_app()"

FROM base as development
ENV FLASK_APP=todo_app/app
ENV FLASK_ENV=development
ENTRYPOINT poetry run flask run --host 0.0.0.0

FROM base as test
ENTRYPOINT poetry run pytest
 


