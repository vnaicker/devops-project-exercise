"""Unit tests for todo_app"""

import pytest
from todo_app.data.view_model import ViewModel, Item

@pytest.fixture
def model():
    item1=Item("1","item1")
    item2=Item("2","item2","Doing")
    item3=Item("2","item3","Done")
    test_items=[item1, item2, item3]
    
    model=ViewModel(test_items)
    return model

def test_view_model_done_property(model: ViewModel):
    
    d_items=model.done_items
    
    assert len(d_items) > 0

def test_view_model_todo_property(model: ViewModel):
    
    td_items=model.todo_items
    
    assert len(td_items) > 0

def test_view_model_doing_property(model: ViewModel):
    
    dn_items=model.doing_items
    
    assert len(dn_items) > 0

@pytest.fixture(scope='module')
def driver():
    opts = webdriver.ChromeOptions()
    opts.add_argument('--headless')
    opts.add_argument('--no-sandbox')
    opts.add_argument('--disable-dev-shm-usage')
    with webdriver.Chrome(options=opts) as driver:
        yield driver
