import os
import pytest
import requests
import mongomock
from todo_app import app
from todo_app.data.view_model import ViewModel, Item
from dotenv import load_dotenv, find_dotenv

@pytest.fixture
def client():
    # Use our test integration config instead of the 'real' version
    file_path = find_dotenv('.env.test')
    load_dotenv(file_path, override=True)

    with mongomock.patch(servers=(('fakemongo.com', 27017),)):
        test_app = app.create_app()
        with test_app.test_client() as client:
            yield client

class StubResponse():
    def __init__(self, fake_response_data):
        self.fake_response_data = fake_response_data

    def json(self):
        return self.fake_response_data

def stub(url, params={}):
    
    fake_card_data = Item('456', 'Test card','To Do', mongomock.utcnow())
    return StubResponse(fake_card_data)

