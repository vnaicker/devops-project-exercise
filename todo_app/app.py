from flask import Flask, render_template, request, redirect, url_for

import os
from dotenv import load_dotenv

from todo_app.flask_config import Config
from todo_app.data import mongodb_items, view_model

def create_app():
    app = Flask(__name__)
    app.config.from_object(Config())

    appKey=os.getenv('APIKey')
    appToken=os.getenv('APIToken')

    connectString=os.getenv('CONNECTION_STRING')
    dbName=os.getenv('DATABASE_NAME')
    collectionName=os.getenv('COLLECTION_NAME')

    @app.route('/')

    @app.route('/index')
    def index():
        items = mongodb_items.get_items(connectString,dbName)
        item_view_model = view_model.ViewModel(items)
        return render_template('index.html', view_model=item_view_model)

    @app.route('/receive_form', methods=['POST'])
    def receive_form():
        form_value=request.form['NewTitle']
        items=mongodb_items.add_item(connectString,dbName,form_value)
        return redirect(url_for('index'))

    @app.route('/save_form', methods=['POST'])
    def save_form():
        if request.method == 'POST':
            form_value=request.form['MoveTitle']
            print(form_value)
            items=mongodb_items.save_item(connectString,dbName,form_value)
        return redirect(url_for('index'))
 
    return app