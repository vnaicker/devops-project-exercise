import requests
import json
import os


base_url = "https://api.trello.com/1/"
appurl = "https://api.trello.com/1/boards/64fdc33df0fcf88f0e782e3b/lists"

class Item:
    def __init__(self, id, name, status = 'To Do'):
        self.id = id
        self.name = name
        self.text = status

    @classmethod
    def from_trello_card(cls, card, list):
        return cls(card['id'], card['name'], list['name'])


def get_items(appKey,appToken):
    """
    Fetches all to-do items from the board.

    Returns:
        list: The list of to-do items.
    """
    list_path = f"boards/{ os.getenv('TRELLO_BOARD_ID') }/lists"
    list_url= base_url+list_path

    QueryParam = {
        'key': 'APIKey',
        'token': 'APITokey',
        'card_fields': 'id,name,text',
        'cards': 'open'
    }
    QueryParam['key']=appKey
    QueryParam['token']=appToken

    r = requests.get(list_url, params=QueryParam)

    response_json = r.json()

    cards = []
    for trello_list in response_json:
        for card in trello_list['cards']:
            card_item=Item.from_trello_card(card, trello_list)
            cards.append(card_item.__dict__)
    
    sorted_cards=sorted(cards, key=lambda x: x.get('text', 0))
    return sorted_cards

def add_item(appKey,appToken,title):
    """
    Adds a new card with the specified title to the list.

    Args:
        title: The title of the item.

    Returns:
        item: The saved item.
    """
    card_url= base_url+"card"

    QueryParam = {
        'key': 'APIKey',
        'token': 'APITokey',
        'idList': '64fdc33df0fcf88f0e782e3d',
        'name': 'name'
    }
    QueryParam['key']=appKey
    QueryParam['token']=appToken
    QueryParam['name']=title

    r=requests.post(card_url,params=QueryParam)
    items = get_items(appKey,appToken)
    return items
    

def save_item(appKey,appToken,item):
    """
    Updates an existing card in the ToDo and move to Done List

    Args:
        item: The card to save.
    """
    card_url= base_url+"cards/"+item

    get_query_param = {
        'key': 'APIKey',
        'token': 'APITokey',
        'id': 'id'
    }

    put_query_param = {
        'key': 'APIKey',
        'token': 'APITokey',
        'idList': '64fdc33df0fcf88f0e782e3f',
    }

    get_query_param['key']=appKey
    get_query_param['token']=appToken
    get_query_param['id']=item

    put_query_param['key']=appKey
    put_query_param['token']=appToken

    r = requests.get(card_url, params=get_query_param)
    rcode = r.status_code

    # Only if we have fetched correctly
    if (rcode == 200):
        rp = requests.put(card_url, params=put_query_param)
    else:
        print("Failed to get Card["+ str(item) +"]: "+ str(rcode))

    items = get_items(appKey,appToken)
    return items
