import pymongo
import pprint
import datetime
import requests
from bson.objectid import ObjectId
import json
import os


class Item:
    def __init__(self, id, name, status = 'To Do', activity = datetime.datetime.now(tz=datetime.timezone.utc)):
        self.id = id
        self.name = name
        self.text = status
        self.lastActivity = activity


    @classmethod
    def from_mangodb_card(cls, card):
        return cls(card['_id'], card['name'],card['text'],card['lastActivity'])


def get_items(connectString,dbName):
    """
    Fetches all to-do items from the board.

    Returns:
        list: The list of to-do items.
    """

    client = pymongo.MongoClient(connectString)
    db = client[dbName]
    collection=db[os.getenv('COLLECTION_NAME')]

    cards = []
    for card in collection.find():
        card_item=Item.from_mangodb_card(card)
        cards.append(card_item.__dict__)
    
    sorted_cards=sorted(cards, key=lambda x: x.get('text', 0))
    return sorted_cards

def add_item(connectString,dbName,title):
    """
    Adds a new card with the specified title to the list.

    Args:
        title: The title of the item.

    Returns:
        item: The saved item.
    """

    client = pymongo.MongoClient(connectString)
    db = client[dbName]
    collection=db[os.getenv('COLLECTION_NAME')]

    newCard = {
        "name": title,
        "text":'To Do',
        "lastActivity": datetime.datetime.now(tz=datetime.timezone.utc)
    }
    
    collection.insert_one(newCard)

    items = get_items(connectString,dbName)
    return items
    

def save_item(connectString,dbName,title):
    """
    Updates an existing card in the ToDo and move to Done List

    Args:
        item: The card to save.
    """

    client = pymongo.MongoClient(connectString)
    db = client[dbName]
    collection=db[os.getenv('COLLECTION_NAME')]

    card = collection.find_one({"name": title})
    card_id_as_str = card['_id']
    collection.update_one({'_id': ObjectId(card_id_as_str)}, {"$set": {"text": 'Done', "lastActivity": datetime.datetime.now(tz=datetime.timezone.utc)}})

    items = get_items(connectString,dbName)
    return items
