from todo_app.data.mongodb_items import Item
#from todo_app.data.trello_items import Item

class ViewModel:
    def __init__(self, items):
        self._items = items
 
    @property
    def items(self):
        return self._items
    
    @property
    def done_items(self):
        d_items = []
        for d_item in self._items:
            if (d_item.__dict__['text'] == 'Done'):
                d_items.append(d_item)
        return d_items
    
    @property
    def todo_items(self):
        td_items = []
        for td_item in self._items:
            if (td_item.__dict__['text'] == 'To Do'):
                td_items.append(td_item)
        return td_items
    
    @property
    def doing_items(self):
        dn_items = []
        for dn_item in self._items:
            if (dn_item.__dict__['text'] == 'Doing'):
                dn_items.append(dn_item)
        return dn_items
    