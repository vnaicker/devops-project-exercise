# DevOps Apprenticeship: Project Exercise

> If you are using GitPod for the project exercise (i.e. you cannot use your local machine) then you'll want to launch a VM using the [following link](https://gitpod.io/#https://github.com/CorndelWithSoftwire/DevOps-Course-Starter). Note this VM comes pre-setup with Python & Poetry pre-installed.

## System Requirements

The project uses poetry for Python to create an isolated environment and manage package dependencies. To prepare your system, ensure you have an official distribution of Python version 3.8+ and install Poetry using one of the following commands (as instructed by the [poetry documentation](https://python-poetry.org/docs/#system-requirements)):

### Poetry installation (Bash)

```bash
curl -sSL https://install.python-poetry.org | python3 -
```

### Poetry installation (PowerShell)

```powershell
(Invoke-WebRequest -Uri https://install.python-poetry.org -UseBasicParsing).Content | py -
```

## Dependencies

The project uses a virtual environment to isolate package dependencies. To create the virtual environment and install required packages, run the following from your preferred shell:

```bash
$ poetry install
```

You'll also need to clone a new `.env` file from the `.env.template` to store local configuration options. This is a one-time operation on first setup:

```bash
$ cp .env.template .env  # (first time only)
```

The `.env` file is used by flask to set environment variables when running `flask run`. This enables things like development mode (which also enables features like hot reloading when you make a file change). There's also a [SECRET_KEY](https://flask.palletsprojects.com/en/1.1.x/config/#SECRET_KEY) variable which is used to encrypt the flask session cookie.

## Running the App

Once the all dependencies have been installed, start the Flask app in development mode within the Poetry environment by running:
```bash
$ poetry run flask run
```

You should see output similar to the following:
```bash
 * Serving Flask app "app" (lazy loading)
 * Environment: development
 * Debug mode: on
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
 * Restarting with fsevents reloader
 * Debugger is active!
 * Debugger PIN: 226-556-590
```
Now visit [`http://localhost:5000/`](http://localhost:5000/) in your web browser to view the app.

We’re going to be using Trello’s API to fetch and save to-do tasks. In order to call their API, you need to first create an account (https://trello.com/signup), then generate an API key and token by following the instructions (https://trello.com/app-key).

Once you have created the token. Follow links below to generate a API Key and token, followel by creating your board and fetching the BOARD ID
https://developers.trello.com/reference#api-key-tokens
https://developer.atlassian.com/cloud/trello/guides/rest-api/api-introduction/#your-first-api-call


## Tests

Tests are present under "todo_app/tests/test_view_model.py"

You can run both unit and integration tests suites using pytest. Run this from the root directory <DevOps-Course-Starter>:

`poetry run pytest`

Or you can run them from VSCode:

Click the conical flask icon on the activity bar on the left edge of VSCode. Click the refresh icon at the top of the panel to rediscover tests. Click the play icon at the top to run all tests. Click the play icon next to a file or test name to run that file or test individually.
* Intellisense annotations for running/debugging each test should also appear above the test functions in the code.
* If test discovery fails, check that "poetry install" ran successfully and that the Python interpreter is selected correctly. See the "setup" section above for details.

## Ansible 

Application can be deployed from Ancible controller node to any Managed node using followng command

`ansible-playbook ./myPlaybook.txt -i myinventory.txt`

Files todoapp.service, .env.j2, myPlaybook.txt & myinventory.txt are present under todoapp on the Git
myinventory.txt should be amended to add new Managed nodes under [myservers] if requried.

## Docker Immage and Container

Make sure Dockerfile contains following steps
1. curl installation
2. poetry installation
3. Copy appilcation code to Docker
4. Depedency resolution via poetry

Build the docker immage as per below

`docker build --target development --tag todo-app:dev .`
`docker build --target production --tag todo-app:prod .`

Use below steps to build a delevoplement and production docker image

`docker run --env-file ./.env -p 5100:5000 --mount "type=bind,source=$(pwd)/todo_app,target=/app/todoapp/todo_app" todo-app:dev`

`docker run --env-file ./.env -p 5000:8000 todo-app:prod`

Following commands can be useful for checks

`docker image ls`
`docker container ls`
`docker container stop <container name>`


## Migration of applicatio to Microsoft Azure
Login to Docker
`docker login`

## Build the docker immage as per below
`docker build --target production --tag todo-app:prod .`

# Push the docker image
`docker push vnaicker/todo-app`

# App needs to be configured in Azure and Execute the below link in browser. 
- Create a Resource -> Web App
- Select your Project Exercise resource group.
- In the “Publish” field, select “Docker Container”
- Under “App Service Plan”, it should default to creating a new one, which is correct. Just change the “Sku and size” to “B1”.
- On the next screen, select Docker Hub in the “Image Source” field, and enter the details of your image.

# Configure following ENV variable as per the Env file
FLASK_APP 
WEBSITES_PORT
TRELLO_BOARD_ID
APIToken
APIKey
SECRET_KEY

# Todo App should be working as expected
# Following webapp has been added to Azure
https://vikramtodoapp.azurewebsites.net/

 # Run test as per below locally and it should still work
 `docker run --env-file .env.test todo-app:test`

# Test your webhook
Take the webhook provided under Deployment Center, add in a backslash to escape the $, and run: curl -dH -X POST "<webhook>"
eg: curl -dH -X POST "https://\$<deployment_username>:<deployment_password>@<webapp_name>.scm.azurewebsites.net/docker/hook"
